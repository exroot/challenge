# Challenge

### Stack

- React
- Express

### Setup

#### 1. Clone

```
https://gitlab.com/exroot/challenge
```

#### 2. Enter to folder

```
cd challenge
```

#### 3. Execution

```
docker-compose up
```

Open http://localhost:3000 with your browser to see the result.
Open http://localhost:4000 to consume de API.
