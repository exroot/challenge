const filesService = require("../services/files.service");

const getData = async (req, res) => {
  try {
    const data = await filesService.getData();
    if (!data) {
      return res.status(404).json("File or data Not Found");
    }
    return res.json(data);
  } catch (error) {
    return res.status(500).send("Internal Error");
  }
};

const getList = async (req, res) => {
  try {
    const data = await filesService.getList();

    if (!data) {
      return res.status(404).json("Data Not Found");
    }
    return res.send(data);
  } catch (error) {
    return res.status(500).send("Internal Error");
  }
};

module.exports = {
  getData,
  getList,
};
