import { GET_DATA, ON_ERROR, SET_LOADING, GET_FILTERED } from "./types";

export const stateReducer = (state, action) => {
  switch (action.type) {
    case GET_DATA:
      return {
        ...state,
        data: action.payload,
        filteredData: action.payload,
      };
    case GET_FILTERED:
      return {
        ...state,
        filteredData: state.data.filter((line) =>
          line.filename.includes(action.payload)
        ),
      };
    case SET_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    case ON_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};
