export const ON_ERROR = "ON_ERROR";
export const GET_FILTERED = "GET_FILTERED";
export const GET_DATA = "GET_DATA";
export const SET_LOADING = "SET_LOADING";
