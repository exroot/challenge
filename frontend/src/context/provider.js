import { useReducer } from "react";
import { stateReducer } from "./reducer";
import { Context } from "./context";
import { transformToArray } from "../utils";
import { getAllFilesData } from "../services";
import { GET_DATA, ON_ERROR, SET_LOADING, GET_FILTERED } from "./types";

const INITIAL_STATE = {
  data: [],
  filteredData: [],
  loading: false,
  error: false,
};

export const StateProvider = ({ children }) => {
  const [state, dispatch] = useReducer(stateReducer, INITIAL_STATE);

  const getData = (filename = "") => {
    dispatch({ type: SET_LOADING, payload: true });
    if (filename.length) {
      /* Buscar por filtrado de filename */
      dispatch({ type: GET_FILTERED, payload: filename });
      dispatch({ type: SET_LOADING, payload: false });
      return;
    }
    getAllFilesData()
      .then((resp) => {
        const lines = transformToArray(resp);
        dispatch({ type: GET_DATA, payload: lines });
        dispatch({ type: SET_LOADING, payload: false });
      })
      .catch((err) => {
        dispatch({ type: ON_ERROR, payload: true });
        dispatch({ type: SET_LOADING, payload: false });
      });
  };

  return (
    <Context.Provider
      value={{
        ...state,
        getData,
      }}
    >
      {children}
    </Context.Provider>
  );
};
