export const getAllFilesData = async () => {
  try {
    const request = await fetch(`http://localhost:4000/files/data`);
    return await request.json();
  } catch (err) {
    console.error("ERROR at getFiles: ", err);
    throw err;
  }
};
