import { useContext, useState, useEffect } from "react";
import { Container, Alert } from "react-bootstrap";
import { Context } from "./context";
import { Navbar, Table, Loading, FilterByFilename } from "./components";

function App() {
  const { data, filteredData, loading, error, getData } = useContext(Context);
  const [filename, setFilename] = useState("");
  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <Navbar />
      {loading && <Loading />}
      <Container
        style={{
          marginTop: "1rem",
        }}
      >
        {error && (
          <Alert variant="danger">
            <Alert.Heading>Error</Alert.Heading>
            <p>An error has occurred</p>
          </Alert>
        )}
        <FilterByFilename filename={filename} setFilename={setFilename} />
        {data && <Table data={filename.length ? filteredData : data} />}
      </Container>
    </>
  );
}

export default App;
